# Vibe Goes Brrr v0.2.1


## Prerequisites

There are a few prerequisites needed for this mod to work:

- **A compatible toy**, [full list here](https://iostindex.com/?filtersChanged=1&filter0ButtplugSupport=4)

- [**Intiface Desktop**](https://intiface.com/desktop/), to connect to your toys

- **A Bluetooth connection**, either built-in or a dongle supporting Bluetooth Low Energy

- [**MelonLoader**](https://melonwiki.xyz/#/?id=requirements), a mod loader for VRChat

- [**UIExpansionKit**](https://github.com/knah/VRCMods/releases/download/updates-2020-11-22/UIExpansionKit.dll), a mod that will let you configure settings in-game (Optional but recommended)

#### Intiface Desktop

Download and install [Intiface Desktop](https://intiface.com/desktop/).

https://github.com/intiface/intiface-desktop/releases/download/v19.0.0/intiface-desktop-19.0.0-win.exe

> Intiface Desktop is an open-source, cross-platform application that acts as a hub for sex hardware access. The mod uses this as a relay to connect to your toys.

#### A Bluetooth connection

You will need some kind of Bluetooth connectivity to connect to your toys. Many computers come with this built-in, but if you're having connectivity issues it might be worth investing in a decent, dedicated Bluetooth dongle. Your Bluetooth connection needs to support Bluetooth Low Energy.

#### MelonLoader

Download the installer and follow the installation instructions at the [MelonLoader Wiki](https://melonwiki.xyz/#/?id=requirements).

https://github.com/HerpDerpinstine/MelonLoader/releases/latest/download/MelonLoader.Installer.exe

**Please be aware that modding your VRChat client is not endorsed by the VRChat team and against their ToS. Use this at your own risk.**

#### UIExpansionKit

Download `UIExpansionKit.dll` and drop it in your VRChat "Mods" folder.

https://github.com/knah/VRCMods/releases/download/updates-2020-11-22/UIExpansionKit.dll

UIExpansionKit is a mod that will add an additional settings menu to your regular VRChat settings menu, allowing you to change mod settings.

This mod is **optional, but highly recommended** so that you may tweak the many settings and quick-toggles provided by Vibe Goes Brrr~


## Installation

Start by dropping `VibeGoesBrrr.dll` into your VRChat "Mods" folder, e.g. `C:\Program Files (x86)\Steam\steamapps\common\VRChat\Mods`.

### Unity Prefabs

In order to set up **touch zones** – the areas on your character which will trigger your toys – you will need to add one or multiple of the included prefabs to your VRC character.

Start by importing the `VibeGoesBrrr.unitypackage` by double-clicking or drag-and-dropping it into your Unity project. Multiple prefabs will be imported into your `Assets/VibeGoesBrrr` folder:

#### VibeSensor

This is the "one size fits all" version of the prefab. You may add one or multiple of these anywhere in your character's armature (though the `Hips` bone is recommended in most cases). Simply drag-and-drop it into a bone in your armature, and adjust it as necessary.

In order to bind a certain VibeSensor to a specific toy, rename the object in the format of "VibeSensor [Toy Name]", e.g. "VibeSensor Hush" or "VibeSensor Max". A partial name is enough. If you're unsure what your toy is named, a list of connected toys will be printed in the Melon Loader console on start. Alternatively, if you only indend on using one toy at a time, leaving the name as "VibeSensor" will trigger *all* connected toys in unison.

The vibration intensity is controlled distance of touch. This means that the deeper something enters the touch zone, the more intense the vibrations will be. To make this work, make sure that the "dots" visible around the edge of the touch zone points **outwards**.

![](docs/2020-12-11-02-10-38-image.png)

You may tweak the `Size` and `Far` parameters of the **Camera** component in order to resize the touch zone. Changing any other parameters is not recommended.

![](docs/2020-12-11-02-13-58-image.png)

##### VibeSensor Hush, VibeSensor Max

These prefabs are provided for convenience for the **Lovense Hush** and the **Lovense Max.** They're set up to fit to the `Hips` bone in your armature, though depending on how your character is fashioned your mileage may vary.


## Connecting

This step is optional, but recommended to make sure your device will connect properly later.

Pair your toy like you usually would any Bluetooth device. Once your device shows up as "Paired", you're ready.

![](docs/2020-12-11-02-41-09-image.png)

![](docs/2020-12-11-02-41-52-image.png)


## Usage

If you've reached this far, you should be good to go! When you start VRChat, the mod should automatically connect to your toys and the touch zones should trigger them.

Important to know is that **the touch zones will only activate by touch from other people and grabbable objects.** If you want to test your touch zones you should join a world that has either of those :)


## Settings

If you installed UIExpansionKit earlier, some extra options should show up in the bottom-right of your VRC settings menu. Clicking "Pin" next to a setting will add an easily accessible toggle to the main menu. Pin both "Touch Vibrations" and "Idle Vibrations" for easy access :)

- **Touch Vibrations**
  Toggles on/off vibrations when a touch zone is activated.

- **Idle Vibrations**
  Toggles on/off idle vibrations at the level set by "Idle Vibration Intensity %"

- **Max Vibration Intensity %**
  Limits the Touch Vibrations to this maximum value.

- **Update Frequency**
  How many times per second the touch zones will be checked for updates. Lower this if you feel it's dragging down your framerate, or increase it if you feel it doesn't update often enough when touched!

![](docs/2020-12-11-02-50-39-image.png)

![](docs/VRChat_y0PSoKDoHk.png)

![](docs/VRChat_lsmUIoO63G.png)


## Troubleshooting

### Nothing happens

- Double-check that your touch zones are set-up correctly and are where you expect them to be. For some avatars the "Hips" bone might not be the best place to attach them.

- Make sure that your device shows up in your PC's Bluetooth settings as "Paired". "Paired" should then change to "Connected" once VRChat starts and your toy is connected.

- If you installed UIExpansionKit, make sure the "Touch Vibrations" setting is enabled in Mod Settings.

- Try toggling the "Idle Vibrations" setting to see if your toy is connected and working.

- Check the Melon Loader console for log messages starting with [VibeGoesBrrr]

- You may test your toy outside of VRChat by opening up Intiface Desktop, clicking "Start Server", and visiting https://playground.buttplug.world

### Disconnections

Sometimes toys may lose connection somewhere along the line from the hardware itself, through the Bluetooth stack and or the Buttplug.io connectivety software. This can happen for various reasons, but in most cases the device will recover within a few seconds.

If your device doesn't automatically reconnect within 10 seconds, you can try restarting it manually.

Most connection problems are caused by the Bluetooth hardware itself. Consider investing a little bit more into a brand name Bluetooth dongle instead of going with the cheapest option.

Another culprit could be your PC's USB controller being overloaded. If you feel like your USB devices are only working intermittently or in weird combinations depending on which device you plug in first, you might need to invest in a USB hub with a built-in dedicated controller to take the load off your PC.

### VRChat crashing

This is a [known issue](https://github.com/buttplugio/buttplug-csharp/issues/664) in buttplug-csharp that may happen if the Bluetooth connection is spotty or if the Bluetooth hardware is a bit shit. If possible, try a different Bluetooth adapter, preferably one that's known to be well compatible with Buttplug.io. Hopefully this will be fixed soon in a future version of the library.

### VRChat lagging after being online for a long time

This is most likely caused by a memory leak present in version 19 of the Intiface Engine and below. Make sure you're on version 20 or above! You can check this by starting Initface Desktop and looking at "Intiface Engine Version" in the settings.

![](docs/intiface-desktop_NJAsp600QI.png)


## Help and Feedback

Feel free to join and post in the [Buttplug.io #virtual-worlds channel on Discord](https://discord.gg/eCtyTMPS2U) if you are having trouble, have bugs to report, or have any other kind of feedback! Make sure you name drop VibeGoesBrrr, since this is a general channel used for a lot of other Buttplug.io integrations as well.


## Changelog

### v0.3.0
- Toys with multiple vibrator motors can now be controlled by different touch zones, by appending a number to the end of the touch zone name. For example, set up two touch zones with the names "VibeSensor Edge 1" and "VibeSensor Edge 2" to separate the motors of the Lovense Edge.
- Added update check on startup

#### v0.2.1
- Many tweaks to hopefully improve connection stability!
- Device scanning is now on a 60 second timer, toggling the "Enable Touch" setting will start a manual scan.
- Toggling the "Enable Touch" setting will automatically restart the server if no devices are connected. If your device disconnects, try this and wait a few seconds!

#### v0.2.0
- Initial release


## Third Party Licenses

### [buttplug-csharp](https://github.com/buttplugio/buttplug-csharp)

```
Buttplug is covered under the following BSD 3-Clause License

Copyright (c) 2017-2020, Nonpolynomial Labs, LLC All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```
