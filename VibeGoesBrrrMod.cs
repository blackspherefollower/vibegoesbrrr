﻿using Buttplug.Client;
using Buttplug.Client.Connectors.WebsocketConnector;
using Buttplug.Core;
using MelonLoader;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnhollowerRuntimeLib;
using UnityEngine;
using static MelonLoader.MelonLogger;

namespace VibeGoesBrrr
{
  public static class BuildInfo
  {
    public const string Name = "VibeGoesBrrr";
    public const string Author = "Jace";
    public const string Version = "0.2.1";
    public const string DownloadLink = "https://gum.co/VibeGoesBrrr";
  }

  public class VibeGoesBrrrMod : MelonMod
  {
    private bool mTouchEnabled = true;
    private bool mIdleEnabled = false;
    private float mIdleIntensity = 5f;
    private float mMaxIntensity = 100f;
    private string mServerURI = "ws://localhost:12345";
    private float mUpdateFreq = 10f;
    private float mScanDuration = 1f;
    private bool mDebugMode = false;

    private AssetBundle mBundle;
    private Shader mShader;
    private GameObject mOrthographicFrustum;
    private GameObject mVibeSensorMax;
    private Process mIntifaceServer;
    private ButtplugClient mClient = null;
    private Task mConnectionTask = Task.CompletedTask;
    private float mNextUpdate = 0;
    private float mNextScan = 0;
    private Dictionary<uint, double[]> mDeviceIntensities = new Dictionary<uint, double[]>();

    public override void OnApplicationStart()
    {
      ClassInjector.RegisterTypeInIl2Cpp<OrthographicDepth>();
#if DEBUG
      ClassInjector.RegisterTypeInIl2Cpp<OrthographicFrustum>();
#endif

      MelonPrefs.RegisterCategory(BuildInfo.Name, "Vibe Goes Brrr~");
      MelonPrefs.RegisterBool(BuildInfo.Name, "TouchEnabled", mTouchEnabled, "Touch Vibrations");
      MelonPrefs.RegisterBool(BuildInfo.Name, "IdleEnabled", mIdleEnabled, "Idle Vibrations");
      MelonPrefs.SetBool(BuildInfo.Name, "IdleEnabled", false);
      MelonPrefs.RegisterFloat(BuildInfo.Name, "IdleIntensity", mIdleIntensity, "Idle Vibration Intensity %");
      MelonPrefs.RegisterFloat(BuildInfo.Name, "MaxIntensity", mMaxIntensity, "Max Vibration Intensity %");
      MelonPrefs.RegisterString(BuildInfo.Name, "ServerURI", mServerURI, "Server URI", hideFromList: true);
      MelonPrefs.RegisterFloat(BuildInfo.Name, "UpdateFreq", mUpdateFreq, "Update Frequency");
      MelonPrefs.RegisterFloat(BuildInfo.Name, "ScanDuration", mScanDuration, "Scan Duration (seconds)", hideFromList: true);
#if DEBUG
      MelonPrefs.RegisterBool(BuildInfo.Name, "DebugMode", mDebugMode, "Debug Mode");
#endif
      OnModSettingsApplied();

      _ = InitializeButtplugClient();
      LoadAssets();
      _ = UpdateCheck();
    }

    public override void OnApplicationQuit()
    {
      if (mClient != null) {
        _ = mClient.DisconnectAsync();
      }
    }

    private async Task InitializeButtplugClient()
    {
      if (mClient != null) {
        await mClient.DisconnectAsync();
      }

      var conn = new ButtplugWebsocketConnector(new System.Uri(mServerURI));
      mClient = new ButtplugClient(BuildInfo.Name, conn);
      mClient.ServerDisconnect += (sender, e) => {
        LogWarning($"Lost connection to Buttplug server. Attempting to reconnect...");
      };
      mClient.DeviceAdded += (sender, e) => {
        mDeviceIntensities.Remove(e.Device.Index);
        var message = $"Device \"{e.Device.Name}\" connected";
        var supporting = new List<string>();
        try {
          var motorCount = e.Device.GetMessageAttributes<Buttplug.Core.Messages.VibrateCmd>().FeatureCount;
          if (motorCount != null) {
            supporting.Add($"{motorCount} vibration motor{(motorCount > 1 ? "s" : "")}");
          }
        } catch (ButtplugDeviceException) { }
        try {
          var rotatorCount = e.Device.GetMessageAttributes<Buttplug.Core.Messages.RotateCmd>().FeatureCount;
          if (rotatorCount != null) {
            supporting.Add($"{rotatorCount} rotation actuator{(rotatorCount > 1 ? "s" : "")}");
          }
        } catch (ButtplugDeviceException) { }
        if (e.Device.Name.Contains("Lovense Max")) {
          supporting.Add("pump");
        }
        if (supporting.Count > 0) {
          message += ", supporting " + String.Join(", ", supporting) + ".";
        }
        Log(message);
#if DEBUG
        Log($"{e.Device.Name} supports the following messages:");
        foreach (var msgInfo in e.Device.AllowedMessages) {
          Log($"- {msgInfo.Key.Name}");
          if (msgInfo.Value.FeatureCount != null) {
            Log($"  - Feature Count: {msgInfo.Value.FeatureCount}");
          }
        }
#endif
      };
      mClient.DeviceRemoved += (sender, e) => {
        mDeviceIntensities.Remove(e.Device.Index);
        Log($"Device \"{e.Device.Name}\" disconnected");
      };
      mClient.ErrorReceived += (sender, e) => {
        LogWarning($"Device error recieved: {e.Exception.Message}");
      };

      try {
        await mClient.ConnectAsync();
      } catch (ButtplugClientConnectorException e) {
        Log($"Starting embedded Intiface server...");
        try {
          StartIntifaceServer();
        } catch (Exception e2) {
          LogError($"Failed to start Intiface server. Is Intiface Desktop properly installed?");
          LogError(e2.Message);
          mClient = null;
          return;
        }
        await mClient.ConnectAsync();
      }

      Log($"Connected to Buttplug server at {mServerURI}");
      mNextScan = Time.time;
    }

    void LoadAssets()
    {
#if DEBUG
      Log("Resources:");
      foreach (var res in Assembly.GetExecutingAssembly().GetManifestResourceNames()) {
        Log($"- {res}");
      }
#endif

      var assetStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("VibeGoesBrrr.Unity.Assets.AssetBundles.assetbundle");
      if (assetStream == null) {
        LogError("Failed to load asset stream");
      }
      var tempStream = new MemoryStream((int)assetStream.Length);
      if (tempStream == null) {
        LogError("Failed to load temp stream");
      }
      assetStream.CopyTo(tempStream);
      mBundle = AssetBundle.LoadFromMemory_Internal(tempStream.ToArray(), 0);
      if (mBundle == null) {
        LogError("Failed to load asset bundle");
      }
      mBundle.hideFlags |= HideFlags.DontUnloadUnusedAsset;

#if DEBUG
      foreach (var name in mBundle.GetAllAssetNames()) {
        Log($"Asset: {name}");
      }
#endif
      mShader = mBundle.LoadAsset_Internal("Assets/VibeGoesBrrr/Internal/OrthographicDepth.shader", Il2CppType.Of<Shader>()).Cast<Shader>();
      if (!mShader) {
        LogError("Failed to load shader");
      }
      mShader.hideFlags |= HideFlags.DontUnloadUnusedAsset;
      var orthographicFrustumMat = mBundle.LoadAsset_Internal("Assets/VibeGoesBrrr/Internal/OrthographicFrustum.mat", Il2CppType.Of<Material>()).Cast<Material>();
      if (!orthographicFrustumMat) {
        LogError("Failed to load OrthographicFrustum material");
      }
      orthographicFrustumMat.hideFlags |= HideFlags.DontUnloadUnusedAsset;
      mOrthographicFrustum = mBundle.LoadAsset_Internal("Assets/VibeGoesBrrr/Internal/OrthographicFrustum.prefab", Il2CppType.Of<GameObject>()).Cast<GameObject>();
      if (!mOrthographicFrustum) {
        LogError("Failed to load OrthographicFrustum prefab");
      }
      mOrthographicFrustum.hideFlags |= HideFlags.DontUnloadUnusedAsset;
      mVibeSensorMax = mBundle.LoadAsset_Internal("Assets/VibeGoesBrrr/VibeSensor Max.prefab", Il2CppType.Of<GameObject>()).Cast<GameObject>();
      mVibeSensorMax.hideFlags |= HideFlags.DontUnloadUnusedAsset;
      mBundle.LoadAllAssets();
    }

    private async Task UpdateCheck()
    {
      try {
        var req = WebRequest.Create(@"https://gitlab.com/api/v4/projects/22979183/releases");
        var response = (HttpWebResponse)(await req.GetResponseAsync());
        var json = new StreamReader(response.GetResponseStream()).ReadToEnd();
        var releases = JArray.Parse(json).OrderByDescending(t => t["commit"]["created_at"]).ToList();
        var latest = new Version(releases[0]["tag_name"].ToString().Replace("v", ""));
        var current = Assembly.GetName().Version;
        if (latest > current) {
          LogWarning("A new version of VibeGoesBrrr~ is avaliable!");
          var links = releases[0]["assets"]["links"];
          if (links.HasValues) {
            LogWarning($"{links[0]["url"]} ({links[0]["name"]})");
          }
          foreach (var release in releases) {
            var version = Version.Parse(release["tag_name"].ToString().Replace("v", ""));
            if (version <= current) {
              break;
            }
            if (release["description"].ToString() != "") {
              LogWarning("");
              LogWarning($"v{version}:");
              foreach (var line in release["description"].ToString().Split('\n')) {
                LogWarning(line);
              }
            }
          }
        } else if (latest < current) {
          LogWarning($"Version {current.ToString(3)} (pre-release)");
        }
      } catch (Exception e) {
        //LogError(e.Message);
        //LogError(e.StackTrace);
      }
    }

    public override void OnLevelWasInitialized(int level)
    {
      if (mClient != null) {
        foreach (var device in mClient.Devices) {
          _ = device.StopDeviceCmd();
        }
      }
    }

    public override void OnUpdate()
    {
      if (mClient == null) return;

      // Reconnect at regular intervals if connection drops
      if (!mClient.Connected) {
        if (mConnectionTask.IsCompleted) {
          mConnectionTask = InitializeButtplugClient();
        }
        mNextUpdate = Time.time + 3;
        return;
      }

      // Scan at regular intervals
      // We can't always scan, because of https://github.com/buttplugio/buttplug-rs/issues/215
      if (Time.time >= mNextScan) {
        _ = Scan();
      }

      if (Time.time < mNextUpdate) return;
      if (Util.LocalPlayer == null) return;

      // Enumerate sensors
      var sensors = GetSensors();
      foreach (var sensor in sensors) {
        // Add depth sampler component if missing 
        var sampler = sensor.GetComponent<OrthographicDepth>();
        if (sampler == null) {
          sampler = sensor.gameObject.AddComponent<OrthographicDepth>();
          sampler.enabled = false;
          sampler.SetShader(mShader);
          Log($"Registered touch zone \"{sensor.name}\"");
        }
#if DEBUG
        // Add debug frustum in debug mode
        var frustum = sensor.GetComponentInChildren<OrthographicFrustum>();
        if (frustum == null && mDebugMode) {
          var obj = GameObject.Instantiate(mOrthographicFrustum, sampler.transform);
          obj.AddComponent<OrthographicFrustum>();
          Log($"Added debug frustum to \"{sensor.name}\" sensor");
        }
#endif
      }

      // Calculate device intensities
      var activeSensors = new HashSet<Camera>();
      foreach (var device in mClient.Devices) {
        uint motorCount;
        try {
          motorCount = (uint)device.GetMessageAttributes<Buttplug.Core.Messages.VibrateCmd>().FeatureCount;
        } catch (ButtplugDeviceException) {
          continue;
        }

        // Match sensors based on device name and gather intensities
        var motorIntensities = new double[(int)motorCount];
        foreach (var (sensor, featureIndex) in MatchDeviceSensors(sensors, device)) {
          var sampler = sensor.GetComponent<OrthographicDepth>();
          sensor.enabled = sampler.enabled = mTouchEnabled;

          // Calculate vibe intensities~
          double intensity = 0f;
          if (mIdleEnabled) {
            intensity += Mathf.Clamp(mIdleIntensity / 100, 0, 1);
          }
          if (mTouchEnabled) {
            intensity += sampler.Depth * Math.Max(0, Math.Min(mMaxIntensity / 100 - intensity, 1));
          }

          // Only vibrate at the maximum intensity of all accumulated affected sensors
          if (featureIndex != null) {
            motorIntensities[(int)featureIndex] = Math.Max(motorIntensities[(int)featureIndex], intensity);
          } else {
            // Vibrate all motors for unindexed sensors
            for (int i = 0; i < motorCount; i++) {
              motorIntensities[i] = Math.Max(motorIntensities[i], intensity);
            }
          }
          activeSensors.Add(sensor);
        }

        // Send device commands
        if (!mDeviceIntensities.ContainsKey(device.Index)) {
          mDeviceIntensities[device.Index] = new double[(int)motorCount];
        }
        if (!motorIntensities.SequenceEqual(mDeviceIntensities[device.Index])) { // Refrain from updating with the same values, since this seems to increase the chance of device hangs
          // VIBRATE!!!
          _ = device.SendVibrateCmd(motorIntensities);
        }
        mDeviceIntensities[device.Index] = motorIntensities;
      }

      // Disable inactive sensors, for performance
      foreach (var sensor in sensors) {
        if (!activeSensors.Contains(sensor)) {
          var sampler = sensor.GetComponent<OrthographicDepth>();
          sensor.enabled = sampler.enabled = false;
        }
      }

      mNextUpdate = Time.time + (1 / mUpdateFreq);
    }

    List<Camera> GetSensors()
    {
      var sensors = new List<Camera>();
#if DEBUG
      var cameras = mDebugMode ? GameObject.FindObjectsOfType<Camera>() : Util.LocalPlayer.GetComponentsInChildren<Camera>();
      foreach (var camera in cameras) {
        if (IsSensor(camera)) {
          sensors.Add(camera);
        }
      }
#else
      foreach (var camera in Util.LocalPlayer.GetComponentsInChildren<Camera>()) {
        if (IsSensor(camera)) {
          sensors.Add(camera);
        }
      }
#endif
      return sensors;
    }

    List<(Camera, int?)> MatchDeviceSensors(List<Camera> sensors, ButtplugClientDevice device)
    {
      var matching = new List<(Camera, int?)>();
      foreach (var sensor in sensors) {
        var vibeSensorMatch = Regex.Match(sensor.name, @"^\s*?Vibe\s*?Sensor\s*(.+?)?\s*(\d+?)?\s*$", RegexOptions.IgnoreCase);
        if (vibeSensorMatch.Success) {
          // Partial match on device name
          if (vibeSensorMatch.Groups[1].Success && !device.Name.ToLower().Contains(vibeSensorMatch.Groups[1].Value.ToLower())) {
            continue;
          }

          // Motor indices are 1-based
          // null index = all motors
          int? motorIndex = null;
          if (device.AllowedMessages.ContainsKey(typeof(Buttplug.Core.Messages.VibrateCmd))) {
            var motorCount = device.GetMessageAttributes<Buttplug.Core.Messages.VibrateCmd>().FeatureCount;
            motorIndex = vibeSensorMatch.Groups[2].Success ? (int?)int.Parse(vibeSensorMatch.Groups[2].Value) - 1 : null;
            if (motorIndex != null && ((int)motorIndex < 0 || (int)motorIndex >= motorCount)) {
              // TODO:
              //LogError($"Invalid motor index \"{motorIndex + 1}\" for touch zone \"{sensor.name}\". The device \"{device.Name}\" reports only having {motorCount} motor{(motorCount > 0 ? "s" : "")}. Make sure you set up your touch zones correctly.");
              continue;
            }
          }

          matching.Add((sensor, motorIndex));
        } else if (sensor.name.StartsWith("HapticsSensor_")) {
          string deviceName = sensor.name.Replace("HapticsSensor_", "").Trim().ToLower();
          int index = int.Parse(deviceName.Substring(0, 1));
          if (index == device.Index) {
            matching.Add((sensor, null));
          }
        }
      }
      return matching;
    }

    bool IsSensor(Camera camera)
    {
      return camera.name.StartsWith("VibeSensor") || camera.name.StartsWith("HapticsSensor_");
    }

    private async Task Scan(bool manuallyTriggered = false)
    {
      if (mClient == null) return;

      // Scan more often if no devices are connected
      if (mClient.Devices.Length == 0) {
        mNextScan = Time.time + 5;
      } else {
        mNextScan = Time.time + 60;
      }

#if DEBUG
      Log("Started scanning...");
#endif
      try {
        _ = mClient.StartScanningAsync();
        await Task.Delay((int)(mScanDuration * 1000));
        try {
          _ = mClient.StopScanningAsync();
        } catch { }
#if DEBUG
        Log("Finished scanning.");
#endif
      } catch (Exception e) {
        LogError($"Scanning failed: {e.Message}");
      } 

      // If scan was manually triggered and it came up empty, kill the server in case it was misbehaving
      if (manuallyTriggered && mClient.Devices.Length == 0 && mIntifaceServer != null) {
        Log("Restarting server since scan came up empty (server might be in a bad state)");
        await mClient.DisconnectAsync(); // Server will be automatically restarted by update loop
        mIntifaceServer.Kill();
      }
    }

    public override void OnModSettingsApplied()
    {
      bool touchEnabled = MelonPrefs.GetBool(BuildInfo.Name, "TouchEnabled");
      if (!mTouchEnabled && touchEnabled) {
        _ = Scan(manuallyTriggered: true);
      }
      mTouchEnabled = touchEnabled;
      bool idleEnabled = MelonPrefs.GetBool(BuildInfo.Name, "IdleEnabled");
      if (!mIdleEnabled && idleEnabled) {
        _ = Scan(manuallyTriggered: true);
      }
      mIdleEnabled = idleEnabled;
      mIdleIntensity = MelonPrefs.GetFloat(BuildInfo.Name, "IdleIntensity");
      mMaxIntensity = MelonPrefs.GetFloat(BuildInfo.Name, "MaxIntensity");
      mServerURI = MelonPrefs.GetString(BuildInfo.Name, "ServerURI");
      mUpdateFreq = MelonPrefs.GetFloat(BuildInfo.Name, "UpdateFreq");
      mScanDuration = MelonPrefs.GetFloat(BuildInfo.Name, "ScanDuration");

#if DEBUG
      bool debugMode = MelonPrefs.GetBool(BuildInfo.Name, "DebugMode");
      if (mDebugMode && !debugMode) {
        foreach (var frustum in GameObject.FindObjectsOfType<OrthographicFrustum>()) {
          Log($"Destroyed frustum on \"{frustum.transform.parent.name}\" sensor");
          GameObject.Destroy(frustum.gameObject);
        }
      }
      mDebugMode = debugMode;
#endif
    }

    void StartIntifaceServer()
    {
      var intifaceConfigDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "IntifaceDesktop");
      var intifaceDir = File.ReadAllText(Path.Combine(intifaceConfigDir, "enginepath.txt"));
      var intifaceCLI = Path.Combine(intifaceDir, "IntifaceCLI.exe");

      mIntifaceServer = new Process();
      mIntifaceServer.StartInfo.FileName = intifaceCLI;
      var port = new Uri(mServerURI).Port;
      mIntifaceServer.StartInfo.Arguments = $"--servername \"{BuildInfo.Name} Server\" --wsinsecureport {port} --log warn";
      mIntifaceServer.StartInfo.UseShellExecute = false;
      mIntifaceServer.Start();
    }
  }
}
