﻿using UnityEngine;
using UnityEngine.Rendering;

[ExecuteInEditMode]
public class PerspectiveDepth : MonoBehaviour
{
  public float Depth = 0f;
  public Shader Shader;
  public AnimationCurve AreaInfluenceCurve;

  private new Camera camera;
  private AsyncGPUReadbackRequest? request;

  void Awake()
  {
    camera = GetComponent<Camera>();
    camera.clearFlags = CameraClearFlags.SolidColor;
    camera.cullingMask = -1;
    camera.backgroundColor = Color.black;
    camera.targetTexture = new RenderTexture(48, 48, 0, RenderTextureFormat.ARGB32);
    // camera.depthTextureMode = DepthTextureMode.Depth;
    camera.depthTextureMode = DepthTextureMode.None;
    // camera.SetReplacementShader(Shader, null);
  }

  void Update()
  {
    if (request == null) {
      request = AsyncGPUReadback.Request(camera.targetTexture);
    } else {
      var req = (AsyncGPUReadbackRequest)request;
      if (req.done) {
        try {
          var data = req.GetData<Color>(0);
          var texture = new Texture2D(camera.targetTexture.width, camera.targetTexture.height, TextureFormat.ARGB32, false);
          texture.LoadRawTextureData(data);

Depth = texture.GetPixel(texture.width / 2, texture.height/ 2).r;
          // var pixels = texture.GetPixels();
          // var closeness = 0f;
          // var n = 0;
          // foreach (var p in pixels) {
          //   if ((1 - p.r) > 0) {
          //     closeness += 1 - p.r;
          //     n++;
          //   }
          // }
          // Depth = n > 0 ? closeness / n : 0;
        } catch (System.InvalidOperationException) { }
        request = null;
      }
    }
  }

  private void OnRenderImage(RenderTexture source, RenderTexture destination) {
    Graphics.Blit(source, destination, new Material(Shader));
  }
}
